$(document).ready(function () {
    function isPropertySupported() {
        var cssFilter = [$(".photo-box img").css('-webkit-filter'), $(".photo-box img").css('filter')];
        for (var i in cssFilter) {
            if (cssFilter[i] == "none" || cssFilter[i] === undefined) {
                cssFilter[i] = 0;
            } else {
                cssFilter[i] = 1;
            }
        }
        if (!(cssFilter[0] + cssFilter[1])) {
            $(".photo-box img").each(function () {
                mOut($(this));
            });
            $(".photo-box img").mouseover(function () {
                var imgSrc = $(this).attr("src");
                var imgSplit = imgSrc.split("gray_").pop();
                $(this).attr("src", "images/" + imgSplit);
                $(this).attr("data-src", "images/" + imgSplit);
            });
            $(".photo-box img").mouseout(function () {
                mOut($(this));
            });
            function mOut(thisElem) {
                var imgSrc = thisElem.attr("data-src");
                var imgSplit = imgSrc.split("/").pop();
                thisElem.attr("data-src", "images/gray_" + imgSplit);
                thisElem.attr("src", "images/gray_" + imgSplit);
            }
        }
    }
    isPropertySupported();
    var svgHeight = $("svg").height();
    if (svgHeight==0) {
    $(".main-header svg").remove();
    $(".main-header").append('<img src="images/top_zeny_logo.png" width="100%" height="100%" />');
    }
});